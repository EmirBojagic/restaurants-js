//changeee
//jos jedan kometas
const ul = document.getElementById("ul");
const orders = {};
const ordersIds = {};
var divPosition = 0;
var increaseId = 0;
var addedRestaurants = [];

const restaurants = [
    {
        id: 0,
        nameOfRestaurant: 'Limenka'
    },
    {
        id: 1,
        nameOfRestaurant: 'Mozaik'
    },
    {
        id: 2,
        nameOfRestaurant: 'Nardi Pub'
    },
    {
        id: 3,
        nameOfRestaurant: 'Njam Njam'
    },
    {
        id: 4,
        nameOfRestaurant: 'Old Story'
    },
    {
        id: 5,
        nameOfRestaurant: 'Pizza Bar'
    },
    {
        id: 6,
        nameOfRestaurant: 'Sezam'
    },
    {
        id: 7,
        nameOfRestaurant: 'Zlatnik'
    }
];

function createHtmlElement(elementType, parentOfElemenet, textOfElement, className, idName) {

    const newElement = document.createElement(elementType);
    newElement.innerHTML = textOfElement;
    newElement.classList.add(className);
    newElement.setAttribute("id", idName);
    parentOfElemenet.appendChild(newElement);
    return newElement;
}

function generateRestaurants() {

    var alreadyAddedRestaurants = getRestaurantsFromStorage();
    if (alreadyAddedRestaurants !== undefined) {
        generateRestaurantsFromStorage(restaurants, alreadyAddedRestaurants)
    }
    else {
        for (let k = 0; k < restaurants.length; k++) {
            genareteRestaurant(restaurants[k])
        }
    }
}

function isRestaurantAlreadyAdded(restaurant, li, spanButton) {

    var restaurantIsAlreadyAdded = localStorage.getItem(restaurant.nameOfRestaurant);
    if (restaurantIsAlreadyAdded) {
        addNewRestaurant(restaurant.id, restaurant.nameOfRestaurant)
        li.removeChild(spanButton);
    }
}

function generateRestaurantsFromStorage(restaurants, alreadyAddedRestaurants) {

    var noAddedRestaurants = [];
    noAddedRestaurants = restaurants;
    for (let i = 0; i < alreadyAddedRestaurants.length; i++) {
        genareteRestaurant(alreadyAddedRestaurants[i])
        noAddedRestaurants = noAddedRestaurants.filter(function (item) {
            return item.id !== alreadyAddedRestaurants[i].id;
        })

    }

    for (let j = 0; j < noAddedRestaurants.length; j++) {
        genareteRestaurant(noAddedRestaurants[j])
    }
}

function genareteRestaurant(restaurant) {

    const li = createHtmlElement("li", ul, null, null);
    createHtmlElement("span", li, restaurant.nameOfRestaurant, 'name', null)
    const spanButton = createHtmlElement('span', li, "Add", 'add', null)

    orders[restaurant.id] = [];
    ordersIds[restaurant.id] = [];

    isRestaurantAlreadyAdded(restaurant, li, spanButton);

    spanButton.onclick = function () {
        addNewRestaurant(restaurant.id, restaurant.nameOfRestaurant)

        var addedRestaurants = localStorage.getItem("addedRestaurants");
        if (addedRestaurants) {
            addedRestaurants = JSON.parse(addedRestaurants);
            addedRestaurants.push(restaurant);
        }
        else {
            addedRestaurants = [restaurant];
        }
        addRestaurantToStorage(addedRestaurants);

        li.removeChild(spanButton);
        li.style.borderColor = "#409fb7";
    }
}

function changePositionOfNewDiv(div) {

    if (divPosition == 0) {
        divPosition += 2;
    }
    else {
        divPosition += 37;
    }

    div.style.left = divPosition + "%";
}

function setItemsToLocalStorage(nameOfRestaurant, orders) {

    localStorage.setItem(nameOfRestaurant, JSON.stringify(orders));

}

function getItemsFromLocalStorage(nameOfRestaurant, id, listInNewDiv) {

    var ordersFromStorage = localStorage.getItem(nameOfRestaurant);
    if (ordersFromStorage) {
        ordersFromStorage = JSON.parse(ordersFromStorage);
        orders[id] = ordersFromStorage;

        for (let i = 0; i < ordersFromStorage.length; i++) {
            createHtmlElement('li', listInNewDiv, null, 'listItem', "id" + increaseId);
            ordersIds[id].push("id" + increaseId);
            listRefresh(listInNewDiv, id, nameOfRestaurant)
            increaseId = increaseId + 1;

        }
    }
}

function listRefresh(listInNewDiv, id, nameOfRestaurant) {

    for (let i = 0; i < ordersIds[id].length; i++) {
        listInNewDiv.removeChild(document.getElementById(ordersIds[id][i]));
        var listItem = createHtmlElement('li', listInNewDiv, null, 'listItem', ordersIds[id][i]);
        createHtmlElement('span', listItem, orders[id][i], 'spanInLi', null);

        const buttonForDeleteItem = createHtmlElement('span', listItem, 'X', 'buttonForDeleteItem', "i" + ordersIds[id][i]);
        buttonForDeleteItem.onclick = function () {
            var thisId = this.id;
            //substring(1) removes first caracter of string
            listInNewDiv.removeChild(document.getElementById((thisId).substring(1)));
            indexForDelete = ordersIds[id].indexOf(thisId.substring(1));
            ordersIds[id] = ordersIds[id].filter(function (item) {
                return item !== thisId.substring(1);
            })
            orders[id].splice(indexForDelete, 1);
            setItemsToLocalStorage(nameOfRestaurant, orders[id]);
        }
    }
}

function createButtonForAddingOrder(div, listInNewDiv, id, nameOfRestaurant) {
    const button = createHtmlElement('span', div, 'Add', 'addOrder', "est" + id.toString());

    button.onclick = function () {

        var valueFromInput = document.getElementById("t" + this.id).value;
        orders[id].push(valueFromInput);
        setItemsToLocalStorage(nameOfRestaurant, orders[id]);
        createHtmlElement('li', listInNewDiv, null, 'listItem', "id" + increaseId);
        ordersIds[id].push("id" + increaseId);
        listRefresh(listInNewDiv, id, nameOfRestaurant);
        increaseId = increaseId + 1;
        document.getElementById("t" + this.id).value = "";

    }
}

function createButtonForDeleteRestaurant(div , nameOfRestaurant) {
    const buttonForDeleteRestaurant = createHtmlElement('span', div, 'Delete', 'deleteRestaurant', null);

    buttonForDeleteRestaurant.onclick = function () {

        parent = document.getElementById("content");
        parent.removeChild(div);
        var oldRestaurantsFromStorage = getRestaurantsFromStorage();
        var newRestaurantForStorage = [];
        for (let i = 0; i < oldRestaurantsFromStorage.length; i++) {
            if (oldRestaurantsFromStorage[i].nameOfRestaurant !== nameOfRestaurant) {
                newRestaurantForStorage.push(oldRestaurantsFromStorage[i]);
            }
        }
        addRestaurantToStorage(newRestaurantForStorage);
        localStorage.removeItem(nameOfRestaurant);
        location.reload();
    }
}

function addRestaurantToStorage(addedRestaurants) {

    localStorage.setItem("addedRestaurants", JSON.stringify(addedRestaurants));
}

function getRestaurantsFromStorage() {

    var storagedRestaurants = localStorage.getItem("addedRestaurants");
    if (storagedRestaurants) {
        storagedRestaurants = JSON.parse(storagedRestaurants);
        return storagedRestaurants;
    }
}

function addNewRestaurant(id, nameOfRestaurant) {

    const div = createHtmlElement('li', content, null, 'newDiv', null);
    changePositionOfNewDiv(div);
    const newDivForTitle = createHtmlElement('div', div, null, 'newDivForTitle', null);
    createHtmlElement('h1', newDivForTitle, restaurants[id].nameOfRestaurant, 'title', null);
    const listInNewDiv = createHtmlElement('ul', div, null, 'listInNewDiv', null);
    const input = createHtmlElement('input', div, null, 'input', "test" + id.toString());
    input.type = "text";

    getItemsFromLocalStorage(nameOfRestaurant, id, listInNewDiv)
    setItemsToLocalStorage(nameOfRestaurant, orders[id]);
    createButtonForAddingOrder(div, listInNewDiv, id, nameOfRestaurant);
    createButtonForDeleteRestaurant(div , nameOfRestaurant);
}

function deleteAllRestaurants() {
    var storagedRestaurants = getRestaurantsFromStorage();
    localStorage.removeItem("addedRestaurants");
    if(storagedRestaurants){
    for (let i = 0; i < storagedRestaurants.length; i++) {
        localStorage.removeItem(storagedRestaurants[i].nameOfRestaurant);
    }
    location.reload();
}
}

generateRestaurants();